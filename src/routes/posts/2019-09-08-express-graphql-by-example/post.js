export default {
  title: `Express-GraphQl by example`,
  tags: ['Express', 'GraphQl', 'node'],
  spoiler: "Learn Graphql with express-graphql and implement catalog with search and pagination.",
  getContent: () => import('./document.mdx'),
}