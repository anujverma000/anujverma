export default {
  title: "Anuj's blog",

  author: "Anuj Verma",
  description: "A personal blog of Anuj Verma",

  // The number of posts to a page on the site index.
  indexPageSize: 5,
}
